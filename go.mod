module gitlab.com/eclaude/gocv-screencapture

go 1.14

require (
	github.com/kbinani/screenshot v0.0.0-20191211154542-3a185f1ce18f
	gocv.io/x/gocv v0.23.0
)
