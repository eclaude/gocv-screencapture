package main

import (
	"flag"

	"github.com/kbinani/screenshot"
	"gocv.io/x/gocv"
)

var (
	screenNumber int
)

func init() {
	flag.IntVar(&screenNumber, "s", 0, "Screen number")
	flag.Parse()
}

func main() {
	window := gocv.NewWindow("Screen")
	defer window.Close()

	bounds := screenshot.GetDisplayBounds(screenNumber)

	for {
		capture, err := screenshot.CaptureRect(bounds)
		if err != nil {
			panic(err)
		}

		img := capture.SubImage(bounds)

		imgCV, err := gocv.ImageToMatRGB(img)
		defer imgCV.Close()
		if err != nil {
			panic(err)
		}

		window.IMShow(imgCV)
		if window.WaitKey(1) == 27 {
			break
		}
	}
}
